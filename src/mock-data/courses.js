export default [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description:  "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Doloribus, at quae dicta? Molestiae, quo impedit sapiente laboriosam corrupti provident mollitia! Accusantium quod, provident magni. Placeat distinctio facere voluptate nulla tenetur?",
		price: 40000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Doloribus, at quae dicta? Molestiae, quo impedit sapiente laboriosam corrupti provident mollitia! Accusantium quod, provident magni. Placeat distinctio facere voluptate nulla tenetur?",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Doloribus, at quae dicta? Molestiae, quo impedit sapiente laboriosam corrupti provident mollitia! Accusantium quod, provident magni. Placeat distinctio facere voluptate nulla tenetur?",
		price: 55000,
		onOffer: true
	}
]