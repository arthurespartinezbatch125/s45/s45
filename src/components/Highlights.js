import React from 'react'
import {
	Row,
	Col,
	Card,
	Button
} from 'react-bootstrap'

export default function Highlights(){
	return(
		<Row className="px-3">
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Learn From Home</Card.Title>
				    <Card.Text>
				      Some quick example text to build on the card title and make up the bulk of
				      the card's content.
				    </Card.Text>
				    <Button variant="primary">Go somewhere</Button>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Study Now, Pay Later</Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum excepturi aperiam iusto nihil quibusdam nemo, delectus quos tenetur, nostrum. Sequi aperiam rerum in vitae. Aliquam soluta ipsum laborum natus consequatur!
				    </Card.Text>
				    <Button variant="primary">Go somewhere</Button>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Be Part of the Community</Card.Title>
				    <Card.Text>
				      Some quick example text to build on the card title and make up the bulk of
				      the card's content.
				    </Card.Text>
				    <Button variant="primary">Go somewhere</Button>
				  </Card.Body>
				</Card>
			</Col>
		</Row>

	)
}
